# tf ec2

## About project

Simple example of creating a aws ec2 instance using terraform in Gitlab CI/CD.

## For You

If you want to use it or mess around with it. Fork this repository and add your own AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY to your CI/CD variables in settings, and it should work.

## Disclaimer

This project does use AWS free tier friendly settings, however that does not mean it is free or will always be free for you. Be smart and check for yourself. Also this project was created for learning purposes.
