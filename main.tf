terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.1.0"
}

provider "aws" {
  region = "us-east-1"
}
resource "aws_security_group" "minecraft_server" {
  name        = "minecraft_server"
  description = "Security group for Minecraft server"

  ingress {
    from_port   = 25565
    to_port     = 25565
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_instance" "minecraft_server" {
  ami             = "ami-0574da719dca65348"
  instance_type   = "t2.micro"
  key_name        = "devops"
  security_groups = [aws_security_group.minecraft_server.name]

  tags = {
    Name = "MC-SERVER-EC2"
  }
}
resource "aws_s3_bucket" "hosts" {
  bucket = "river-mc-bucket"
}
resource "aws_s3_object" "object" {
  bucket  = aws_s3_bucket.hosts.bucket
  key     = "hosts.yaml"
  content = "[ec2]\n${aws_instance.minecraft_server.public_ip} ansible_user=ubuntu"
}